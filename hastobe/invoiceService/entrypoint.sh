#!/bin/sh

# gives time for other services to boot before the application
echo "The application will start in 10s..." && sleep 10
exec java -jar invoiceService.jar
