package com.invoice;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.invoice.dtos.*;

import java.nio.charset.Charset;
import java.sql.Timestamp;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InvoiceControllerTest {
    @Autowired
    private MockMvc mvc;
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Order(1)
    @Test
    public void getInvoiceTest() throws Exception {
        Timestamp timestamp = new Timestamp(1652724658908l);
        Timestamp timestamp2 = new Timestamp(1652734676752l);
        ChargingProcess chargingProcess = new ChargingProcess();
        Rate rate = new Rate();
        CDR cdr = new CDR();
        rate.setEnergy(0.3);
        rate.setTime(2.0);
        rate.setTransaction(1.0);
        chargingProcess.setRate(rate);
        cdr.setTimestampStart(timestamp);
        cdr.setMeterStart(1204307L);
        cdr.setTimestampStop(timestamp2);
        cdr.setMeterStop(1215230l);
        chargingProcess.setCdr(cdr);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(chargingProcess);

        mvc.perform(MockMvcRequestBuilders.post("/invoice/calculate")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", isA(JSONArray.class)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.overall").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.components").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.components.energy").isNotEmpty());
    }
}
