package com.invoice;
import com.invoice.dtos.CDR;
import com.invoice.dtos.ChargingProcess;
import com.invoice.dtos.Invoice;
import com.invoice.dtos.Rate;
import com.invoice.service.impl.CalculateInvoiceServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.sql.Timestamp;

import static org.junit.Assert.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalculateInvoiceServiceTest {

    private ChargingProcess chargingProcess;

    @InjectMocks
    CalculateInvoiceServiceImpl calculateInvoiceService;

    @BeforeEach
    void initUseCase() {
        calculateInvoiceService = new CalculateInvoiceServiceImpl();
        chargingProcess = new ChargingProcess();
        Timestamp timestamp = new Timestamp(1652724658908l);
        Timestamp timestamp2 = new Timestamp(1652734676752l);
        Rate rate = new Rate();
        CDR cdr = new CDR();
        rate.setEnergy(0.3);
        rate.setTime(2.0);
        rate.setTransaction(1.0);
        chargingProcess.setRate(rate);
        cdr.setTimestampStart(timestamp);
        cdr.setMeterStart(1204307L);
        cdr.setTimestampStop(timestamp2);
        cdr.setMeterStop(1215230l);
        chargingProcess.setCdr(cdr);
    }

    @Order(1)
    @Test
    public void calculateTest() throws Exception {
        Invoice invoice = calculateInvoiceService.calculate(chargingProcess);
        assertNotNull(invoice);
        assertNotNull(invoice.getOverall());
        assertNotNull(invoice.getComponents());
        assertNotNull(invoice.getComponents().getTime());
        assertNotNull(invoice.getComponents().getEnergy());
        assertNotNull(invoice.getComponents().getTransaction());
        Assert.assertEquals(Double.valueOf(9.84),invoice.getOverall());
        Assert.assertEquals(Double.valueOf(5.565),invoice.getComponents().getTime());
        Assert.assertEquals(Double.valueOf(3.277),invoice.getComponents().getEnergy());
        Assert.assertEquals(Double.valueOf(1),invoice.getComponents().getTransaction());
    }

}
