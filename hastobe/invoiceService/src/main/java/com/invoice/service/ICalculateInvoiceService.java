package com.invoice.service;

import com.invoice.dtos.*;

//This class is an Interface for CalculateInvoiceServiceImpl service
public interface ICalculateInvoiceService {
    Invoice calculate(ChargingProcess chargingProcess);
}
