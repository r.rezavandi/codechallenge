package com.invoice.service.impl;

import com.invoice.dtos.*;
import com.invoice.service.ICalculateInvoiceService;
import org.decimal4j.util.DoubleRounder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;;
import java.util.TimeZone;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
//This class implement ICalculateInvoiceService service and calculate Invoice amount
public class CalculateInvoiceServiceImpl implements ICalculateInvoiceService {

//   this method is main method that get input from controller
//   and pass each necessary part of them to other method to process on it
    @Override
    public Invoice calculate(ChargingProcess chargingProcess) {
        Rate baseRate = chargingProcess.getRate();
        CDR driverCDR = chargingProcess.getCdr();
        Invoice invoice = new Invoice();
        Rate components = calculateComponents(driverCDR, baseRate);
        Double overall = calculateOverall(components);
        invoice.setComponents(components);
        invoice.setOverall(overall);
        return invoice;
    }

    //    This method get Rate as input and just return sum value of Rate's variables as overall
    private Double calculateOverall(Rate components) {
        Double overall = components.getEnergy() + components.getTime() + components.getTransaction(); //7.04
        return checkForRound(overall, 2);
    }

    //    This method get base Rate and CDR to calculate ech part of component
    private Rate calculateComponents(CDR driverCDR, Rate baseRate) {
        Double energyPrice = calculateEnergyKWh(driverCDR.getMeterStart(), driverCDR.getMeterStop(), baseRate.getEnergy());
        Double chargingHourPrice = calculateChargingHour(driverCDR.getTimestampStart(), driverCDR.getTimestampStop(), baseRate.getTime());
        Double transactionPrice = baseRate.getTransaction();
        Rate components = new Rate();
        components.setEnergy(energyPrice);
        components.setTime(chargingHourPrice);
        components.setTransaction(transactionPrice);
        return components;
    }

    //    This method calculate the difference between start and stop time and calculate charging price according to basic hour price
    private Double calculateChargingHour(Timestamp timestampStart, Timestamp timestampStop, Double perHourPrice) {
        long milliseconds = timestampStop.getTime() - timestampStart.getTime();
        Double seconds = (double) milliseconds / 1000;
        Double hours = seconds / 3600;
        Double chargingPrice = hours * perHourPrice; //2.767
        return checkForRound(chargingPrice, 3);
    }

    //    This method calculate the difference between start and stop meterEnergy and calculate charging price according to energy price per KWH
    private Double calculateEnergyKWh(Long meterStart, Long meterStop, Double energyPriceKWH) {
        Long presumedEnergy = meterStop - meterStart;
        Double energyPrice = (presumedEnergy * energyPriceKWH) / 1000; //3.277
        return checkForRound(energyPrice, 3);
    }

    //    This method check input values base on input precision to decided they need to be round or not then return value
    private Double checkForRound(Double number, int precision) {
        String[] split = number.toString().split("\\.");
        if (split[1].length() > 2) {
            return DoubleRounder.round(number, precision);
        } else if (split[1].length() == 1) {
            return Double.valueOf(Math.round(number));
        } else
            return number;
    }

//     This method just convert a Timestamp to Time base on ISO8601,
//    this format is very similar to sql Timestamp, but it does not use here,
//    because when You declare your variable base on this type you can put also timestamp whit ISO8601 format.
    private String convertToIso8601(Timestamp timestamp) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        String timeAsISO = df.format(timestamp);
        return timeAsISO;
    }

}
