package com.invoice;


import com.invoice.dtos.ChargingProcess;
import com.invoice.dtos.Invoice;
import com.invoice.service.ICalculateInvoiceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;


//This a class for implement rest services for /invoice/ path
@AllArgsConstructor
@RestController
@RequestMapping(value = "/invoice/")
@CrossOrigin
public class InvoiceController {

    private ICalculateInvoiceService calculatorService;

//    this method get ChargingProcess as input and pass it to service layer and return response
//    there are used some Swagger annotations to handle response codes
    @ApiOperation(value = "calculate invoice")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully calculate"),
            @ApiResponse(code = 404, message = "not found"),
    })
    @RequestMapping(value = "/calculate", method = RequestMethod.POST)
    public ResponseEntity<Invoice> getInvoice(@RequestBody ChargingProcess chargingProcess) {
        Invoice response = calculatorService.calculate(chargingProcess);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}


