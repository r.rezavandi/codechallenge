package com.invoice.dtos;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

//In this DTO we declare some properties and use LOMBOK @Data to generate getter, setter, to string ,....
// and also use some swagger annotations , javax validation to validate
@Data
public class Rate {
    @NotNull(message = "energy could not be Null!")
    @NotEmpty(message = "energy could not be empty!")
    @ApiModelProperty(required = true, hidden = false)
    private Double energy;
    @NotNull(message = "time could not be Null!")
    @NotEmpty(message = "time could not be empty!")
    @ApiModelProperty(required = true, hidden = false)
    private Double time;
    @NotNull(message = "transaction could not be Null!")
    @NotEmpty(message = "transaction could not be empty!")
    @ApiModelProperty(required = true, hidden = false)
    private Double transaction;
}
