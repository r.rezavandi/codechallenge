package com.csms;

import com.csms.dtos.ChargingProcess;
import com.csms.dtos.Invoice;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lombok.AllArgsConstructor;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

//This a class for implement rest services for /csms-list path
@AllArgsConstructor
@RestController
@RequestMapping(value = "/csms-list")
@CrossOrigin
public class CsmsController {
    //    In this line we initialize Invoice service
    //    and with using @AllArgsConstructor on top of class we do not need to implement constructor for it
    @Autowired
    InvoiceService invoiceService;
    private final String CSMS_SERVICE = "csmsService";


//    this method get ChargingProcess as input and pass it to invoiceService layer and return response
//    there are used some Swagger annotations to handle response codes
//    also we used CircuitBreaker to control Fall back according to default configuration on application.yml file
    @RequestMapping(value = "/rate", method = RequestMethod.POST)
    @ApiOperation(value = " get invoice")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully calculate"),
            @ApiResponse(code = 404, message = "not found"),
    })
    @CircuitBreaker(name = CSMS_SERVICE, fallbackMethod = "invoiceFallback")
    public ResponseEntity<Invoice> getInvoice(@RequestBody ChargingProcess chargingProcess) {
        Invoice response =invoiceService.calculateInvoice(chargingProcess);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //This method implemented to Fallback times to show this message to users
    public ResponseEntity<String> invoiceFallback(Exception e) {
        return new ResponseEntity<String>("Invoice service is down, this is alternative service", HttpStatus.OK);
    }
}







