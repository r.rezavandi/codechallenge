package com.csms;


import com.csms.common.FeignConfig;
import com.csms.dtos.ChargingProcess;
import com.csms.dtos.Invoice;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

//This class by using FeignClient provide internal service call for CSMS service from Invoice service
@FeignClient(name = "invoice", url = "http://localhost:8080/invoiceservice/invoice/", configuration = FeignConfig.class)

public interface InvoiceService {
//   In this method we call specific service from Invoice Service and pass parameter as input to it
    @RequestMapping(value="/calculate", method = RequestMethod.POST)
    Invoice calculateInvoice(@RequestBody ChargingProcess chargingProcess);


}
