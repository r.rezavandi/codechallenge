package com.csms.dtos;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.convert.DurationFormat;
import org.springframework.boot.convert.DurationStyle;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

//In this DTO we declare some properties and use LOMBOK @Data to generate getter, setter, to string ,....
// and also use some swagger annotations , javax validation to validate
@Data
public class CDR {
    @NotNull(message = "meterStart could not be Null!")
    @ApiModelProperty(required = true, hidden = false)
    @NotEmpty(message = "meterStart could not be empty!")
    private Long meterStart;

    @NotNull(message = "timestampStart could not be Null!")
    @NotEmpty(message = "timestampStart could not be empty!")
    @ApiModelProperty(required = true, hidden = false)
    @DurationFormat(DurationStyle.ISO8601)
    private Timestamp timestampStart;

    @NotNull(message = "meterStop could not be Null!")
    @NotEmpty(message = "meterStop could not be empty!")
    @ApiModelProperty(required = true, hidden = false)
    private Long meterStop;

    @NotNull(message = "timestampStop could not be Null!")
    @NotEmpty(message = "timestampStop could not be empty!")
    @ApiModelProperty(required = true, hidden = false)
    @DurationFormat(DurationStyle.ISO8601)
    private Timestamp timestampStop;

}
