package com.csms.dtos;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

//In this DTO we declare some properties and use LOMBOK @Data to generate getter, setter, to string ,....
// and also use some swagger annotations , javax validation to validate
@Data
public class ChargingProcess {
    @NotNull(message = "rate could not be Null!")
    @NotEmpty(message = "rate could not be empty!")
    @ApiModelProperty(required = true, hidden = false)
    private Rate rate;
    @NotNull(message = "cdr could not be Null!")
    @NotEmpty(message = "cdr could not be empty!")
    @ApiModelProperty(required = true, hidden = false)
    private CDR cdr;
}
