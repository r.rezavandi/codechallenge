package com.csms.dtos;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

//In this DTO we declare some properties and use LOMBOK @Data to generate getter, setter, to string ,....
// and also use some swagger annotations , javax validation to validate
@Data
public class Invoice {
    @NotNull(message = "Overall could not be Null!")
    @NotEmpty(message = "Overall could not be empty!")
    @NotBlank(message = "Overall could not be blank!")
    @ApiModelProperty(required = true, hidden = false)
    private Double overall;
    @NotNull(message = "components could not be Null!")
    @NotEmpty(message = "components could not be empty!")
    @NotBlank(message = "components could not be blank!")
    @ApiModelProperty(required = true, hidden = false)
    private Rate components;
}
