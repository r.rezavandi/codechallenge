package com.csms;

import com.csms.dtos.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isA;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CsmsControllerTest {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Autowired
    private MockMvc mvc ;

    @Mock
    InvoiceService invoiceService;

    @Order(1)
    @Test
    public void getInvoiceTest() throws Exception
    {   Timestamp timestamp = new Timestamp(1652724658908l);
        Timestamp timestamp2 = new Timestamp(1652734676752l);
        ChargingProcess chargingProcess = new ChargingProcess();
        Rate rate = new Rate();
        CDR cdr = new CDR();
        rate.setEnergy(0.3);
        rate.setTime(2.0);
        rate.setTransaction(1.0);
        chargingProcess.setRate(rate);
        cdr.setTimestampStart(timestamp);
        cdr.setMeterStart(1204307L);
        cdr.setTimestampStop(timestamp2);
        cdr.setMeterStop(1215230l);
        chargingProcess.setCdr(cdr);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(chargingProcess);

        Invoice invoice = new Invoice();
        Rate components = new Rate();
        invoice.setOverall(9.84);
        components.setTransaction(1.0);
        components.setEnergy(3.277);
        components.setTime(5.565);
        invoice.setComponents(components);

        when(invoiceService.calculateInvoice(any(ChargingProcess.class))).thenReturn(invoice);
        mvc.perform(MockMvcRequestBuilders.post("/csms-list/rate")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", isA(JSONArray.class)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.*", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.overall").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.components").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.components.energy").isNotEmpty());
    }
}
