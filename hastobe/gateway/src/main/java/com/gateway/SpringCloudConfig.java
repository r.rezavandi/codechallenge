package com.gateway;


import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


//This class build route locator according on request path
@Configuration
public class SpringCloudConfig {
    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/invoiceservice/**")
                        .uri("lb://invoice-service")
                )

                .route(r -> r.path("/csmsservice/**")
                        .uri("lb://csms-service")
                )
                .build();
    }

}